============
Fred-fileman
============

Fred-fileman is a service for file management.

Learn more about the project and our community on the `FRED's home page`__

.. _FRED: https://fred.nic.cz

__ FRED_


Configuration
=============

Fileman searches for configuration files in following order:

1. Configuration file set by ``FILEMAN_CONFIG`` environment variable.
2. Configuration file set by ``--config`` option of a command.
3. ``~/.fred/fileman.conf``
4. ``/etc/fred/fileman.conf``

The configuration file is in YAML format with following options:

``db_connection``
-----------------
A connection string to database.
See https://docs.sqlalchemy.org/core/engines.html#database-urls for possible values.
Default is ``sqlite:///:memory:``.

``db_echo``
-----------
Whether to log SQL statements.
Default is ``False``.

``db_poolclass``
----------------
Dotted path to a pool class.
See https://docs.sqlalchemy.org/core/pooling.html for details.
Default is ``None``, i.e. use SQLAlchemy default pool class.

``grpc_port``
-------------
Port to which the fileman gRPC service will bind and listen.
Default is 50051.

``logging``
-----------
Logging configuration, see https://docs.python.org/3.8/library/logging.config.html for possible values.
Default is ``None``, i.e. use ``logging`` defaults.

``max_workers``
---------------
Maximum number of workers for gRPC server, see https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor.
Default is ``None``, i.e. use Python library default.
Implemented to avoid problems with too many workers on many-core machines under Python < 3.8.

``sentry``
----------
Sentry settings in a nested structure.
May contain following keys:

 * ``dsn`` contains data source name (DSN), see https://docs.sentry.io/product/sentry-basics/dsn-explainer/.
   If not provided, Sentry client is not set up.
 * ``environment`` may contain an environment identifier.
 * ``ca_certs`` may contain a path to CA certificates file.

``storage_root``
----------------
Path to the root of the file system storage.
Default is ``/var/lib/fileman``.
