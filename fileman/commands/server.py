#!/usr/bin/python3
"""Run fileman gRPC server.

Usage: fred-fileman-server [options]
       fred-fileman-server -h | --help
       fred-fileman-server --version

Options:
  -h, --help                show this help message and exit
  --version                 show program's version number and exit
  --config=FILE             set custom config file
  -p, --port=PORT           set custom port
  --connection=CONNECTION   set custom database connection string
  --poolclass=POOLCLASS     set custom pool class
  --storage=STORAGE         set custom storage root
  --max-workers=NUM         set maximum number of workers
"""

from collections.abc import Iterable
from concurrent.futures import ThreadPoolExecutor
from importlib import import_module
from time import sleep
from typing import Optional

import grpc
from docopt import docopt
from fred_api.fileman.service_fileman_grpc_pb2_grpc import add_FileServicer_to_server
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import fileman
from fileman.service import FileServicer
from fileman.settings import APPLICATION

# Mapping command line options to setting names.
_OPTIONS_MAPPING = {
    "--connection": "db_connection",
    "--max-workers": "max_workers",
    "--poolclass": "db_poolclass",
    "--port": "grpc_port",
    "--storage": "storage_root",
}


def main(argv: Optional[Iterable[str]] = None) -> None:
    """Start the server."""
    options = docopt(__doc__, argv=argv, version=fileman.__version__)
    APPLICATION.load(options["--config"])
    for option, setting in _OPTIONS_MAPPING.items():
        value = options.get(option)
        if value:
            setattr(APPLICATION.settings, setting, value)

    if APPLICATION.settings.max_workers is None:
        max_workers = None
    else:
        max_workers = int(APPLICATION.settings.max_workers)
    server = grpc.server(ThreadPoolExecutor(max_workers))
    if APPLICATION.settings.db_poolclass:
        try:
            module_name, class_name = APPLICATION.settings.db_poolclass.rsplit(".", 1)
            kwargs = {"poolclass": getattr(import_module(module_name), class_name)}
        except (ImportError, AttributeError, ValueError):
            exit("'{}' was not found.".format(APPLICATION.settings.db_poolclass))
    else:
        kwargs = {}
    engine = create_engine(APPLICATION.settings.db_connection, **kwargs, future=True)
    add_FileServicer_to_server(
        FileServicer(APPLICATION.settings.storage_root, sessionmaker(bind=engine, future=True)), server
    )
    server.add_insecure_port("[::]:{port}".format(port=APPLICATION.settings.grpc_port))
    server.start()
    # Sadly this is the recommended way to keep the server running.
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    main()
