"""Models for fileman."""

import uuid
from typing import Any

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import as_declarative
from sqlalchemy.sql import expression, func
from sqlalchemy_utc import UtcDateTime
from sqlalchemy_utils import UUIDType


@as_declarative()
class BaseModel:
    """Base class for fileman models."""

    id = Column(Integer, primary_key=True)

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        # Define the __init__ to provide definition for mypy
        super().__init__(*args, **kwargs)  # pragma: no cover


class File(BaseModel):
    """Model representation of FRED DB table 'files'.

    Attributes:
        uuid: The file UUID.
        create_datetime: Date and time of the file creation.
        path: Unique identifier of the file in a storage.
        mimetype: The file MIME type.
        size: File size in bytes.
        name: Display name of the file.
    """

    __tablename__ = "files"

    uuid = Column(UUIDType(binary=False), nullable=False, unique=True, default=uuid.uuid4)
    create_datetime = Column(UtcDateTime(timezone=False), name="crdate", nullable=False, server_default=func.now())
    path = Column(String(length=300), nullable=False)
    mimetype = Column(String(length=300), nullable=False, server_default=expression.text("'application/octet-stream'"))
    size = Column(Integer(), name="filesize", nullable=False)
    name = Column(String(length=300), nullable=False)

    def __repr__(self) -> str:
        return "<{} {} ({} {})>".format(type(self).__name__, self.uuid, self.name, self.path)
