"""Fileman settings management."""

import logging
from typing import Optional

from setapp import BaseApplication, BaseSettings, SettingsConfigDict

try:  # pragma: no cover
    import sentry_sdk
    from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
except ImportError:
    sentry_sdk = None
    SqlalchemyIntegration = None

_LOGGER = logging.getLogger(__name__)
ENVIRON_NAME = "FILEMAN_CONFIG"
CONFIG_FILES = ("~/.fred/fileman.conf", "/etc/fred/fileman.conf")


class Settings(BaseSettings):
    """Actual settings."""

    model_config = SettingsConfigDict(env_prefix="FILEMAN_", config_files=CONFIG_FILES, config_environ=ENVIRON_NAME)

    db_connection: str = "sqlite:///:memory:"
    db_echo: bool = False
    db_poolclass: Optional[str] = None
    grpc_port: int = 50051
    max_workers: Optional[int] = None
    storage_root: str = "/var/lib/fileman"


class Application(BaseApplication[Settings]):
    """Represent application settings and basic setup."""


APPLICATION = Application(Settings)
