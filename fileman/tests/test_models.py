from unittest import TestCase
from uuid import UUID

from fileman.models import File


class FileTest(TestCase):
    def test_repr(self):
        uuid = UUID(int=42)
        afile = File(uuid=uuid, path="person/files/rimmer.xml", name="rimmer.dat")
        self.assertEqual(repr(afile), "<File {} (rimmer.dat person/files/rimmer.xml)>".format(uuid))
