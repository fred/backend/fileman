from functools import partial

from setapp.utils import override_settings as _override_settings

from fileman.settings import APPLICATION

override_settings = partial(_override_settings, APPLICATION)
