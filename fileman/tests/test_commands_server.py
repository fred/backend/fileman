from concurrent.futures import ThreadPoolExecutor
from typing import Optional
from unittest import TestCase
from unittest.mock import call, patch

from grpc._utilities import DictionaryGenericHandler
from sqlalchemy.pool import NullPool
from testfixtures import Comparison

from fileman.commands import server
from fileman.settings import Application, Settings

from .utils import override_settings


class TestApplication(Application):
    def load(self, config_file: Optional[str] = None) -> None:
        """Do nothing. Overrides are used instead."""


@override_settings(db_connection="sqlite:///:memory:")
class ServiceMainTest(TestCase):
    def setUp(self):
        test_app = TestApplication(Settings)
        test_app.settings = Settings()
        patcher = patch("fileman.commands.server.APPLICATION", new=test_app)
        self.addCleanup(patcher.stop)
        patcher.start()

    def test_main(self):
        with patch("fileman.commands.server.sleep", side_effect=KeyboardInterrupt):
            with patch("fileman.commands.server.grpc") as grpc_mock:
                server.main([])

        calls = [
            call.server(Comparison(ThreadPoolExecutor)),
            call.server().add_generic_rpc_handlers((Comparison(DictionaryGenericHandler),)),
            call.server().add_insecure_port("[::]:50051"),
            call.server().start(),
            call.server().stop(0),
        ]
        self.assertEqual(grpc_mock.mock_calls, calls)

    def test_main_max_workers(self):
        with patch("fileman.commands.server.sleep", side_effect=KeyboardInterrupt):
            with patch("fileman.commands.server.grpc") as grpc_mock:
                server.main(["--max-workers", "7"])

        pool = grpc_mock.mock_calls[0][1][0]
        self.assertEqual(pool._max_workers, 7)

    def test_main_poolclass(self):
        with patch("fileman.commands.server.sleep", side_effect=KeyboardInterrupt):
            with patch("fileman.commands.server.grpc"):
                with patch("fileman.commands.server.create_engine") as wrapped_create_engine:
                    server.main(["--poolclass", "sqlalchemy.pool.NullPool"])

        self.assertEqual(
            wrapped_create_engine.mock_calls, [call("sqlite:///:memory:", poolclass=NullPool, future=True)]
        )

    def test_main_poolclass_invalid(self):
        with self.assertRaisesRegex(SystemExit, "'fileman.does_not_exist.UnknownPool' was not found."):
            server.main(["--poolclass", "fileman.does_not_exist.UnknownPool"])
