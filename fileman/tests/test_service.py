import os
import sys
from collections.abc import Generator, Iterable
from contextlib import contextmanager
from datetime import datetime
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Union, cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel
from uuid import UUID

from fred_api.fileman.service_fileman_grpc_pb2 import (
    CreateReply,
    CreateRequest,
    ReadReply,
    ReadRequest,
    StatReply,
    StatRequest,
)
from freezegun import freeze_time
from grpc import StatusCode
from pytz import UTC
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from testfixtures import LogCapture, StringComparison

from fileman.models import BaseModel, File
from fileman.service import FileServicer


@contextmanager
def non_root_user() -> Generator:
    """Switch to a non root user."""
    uid = os.getuid()
    try:
        # If user is root, change user.
        if not uid:  # pragma: no cover
            os.seteuid(1000)
        yield
    finally:
        # Restore user.
        os.seteuid(uid)


class FileServicerTest(TestCase):
    content = b"Rimmer is a smeg head!"

    def setUp(self):
        # We mess the database structure in some tests. Create database every time.
        self.engine = create_engine("sqlite:///:memory:", future=True)
        BaseModel.metadata.create_all(self.engine)  # type: ignore[attr-defined]
        self.connection = self.engine.connect()
        self.session_cls = sessionmaker(bind=self.connection, future=True)
        self.session = self.session_cls()

        self.tmp_dir = TemporaryDirectory()
        self.service = FileServicer(self.tmp_dir.name, self.session_cls)
        self.log_handler = LogCapture("fileman", propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.tmp_dir.cleanup()

    def create_storage_file(self, path: Union[Path, str]) -> None:
        """Create empty file in storage."""
        # TODO: Drop 'str', once 3.6 is the minimal python supported.
        full_path = Path(self.tmp_dir.name) / str(path)
        full_path.parent.mkdir(parents=True, exist_ok=True)
        full_path.touch()

    def test_stat_empty(self):
        request = StatRequest()
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.stat(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.NOT_FOUND, "File '' not found")])

    def test_stat_db_error(self):
        # Test DB error occurs on database level - dropped table
        with self.engine.begin() as conn:
            BaseModel.metadata.drop_all(bind=conn)  # type: ignore[attr-defined]
        request = StatRequest()
        request.uid.value = str(UUID(int=42))
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.stat(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(
            context.mock_calls, [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))]
        )

    def _test_stat_not_found(self, raw_id: str) -> None:
        request = StatRequest()
        request.uid.value = raw_id
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.stat(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.NOT_FOUND, f"File '{raw_id}' not found")])

    def test_stat_not_found(self):
        # Test ID can be resolved, but file does not exist.
        self._test_stat_not_found("255")

    def test_stat_invalid(self):
        # Test ID can't be resolved.
        self._test_stat_not_found("invalid")

    def test_stat(self):
        uuid = UUID(int=42)
        name = "example.txt"
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        mimetype = "text/plain"
        size = 1024
        afile = File(uuid=uuid, create_datetime=create_datetime, path="/", mimetype=mimetype, size=size, name=name)
        self.session.add(afile)
        self.session.commit()

        request = StatRequest()
        request.uid.value = str(uuid)
        result = self.service.stat(request, sentinel.context)

        reply = StatReply()
        reply.data.create_datetime.FromDatetime(create_datetime)
        reply.data.size = size
        reply.data.mimetype = mimetype
        reply.data.name = name
        self.assertEqual(result, reply)

    def test_stat_numeric_id(self):
        id = 42
        name = "example.txt"
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        mimetype = "text/plain"
        size = 1024
        afile = File(id=id, create_datetime=create_datetime, path="/", mimetype=mimetype, size=size, name=name)
        self.session.add(afile)
        self.session.commit()

        request = StatRequest()
        request.uid.value = str(id)
        result = self.service.stat(request, sentinel.context)

        reply = StatReply()
        reply.data.create_datetime.FromDatetime(create_datetime)
        reply.data.size = size
        reply.data.mimetype = mimetype
        reply.data.name = name
        self.assertEqual(result, reply)

    def test_read_empty(self):
        request = ReadRequest()
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.read(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.NOT_FOUND, "File '' not found")])

    def test_read_db_error(self):
        # Test DB error occurs on database level - dropped table
        with self.engine.begin() as conn:
            BaseModel.metadata.drop_all(bind=conn)  # type: ignore[attr-defined]
        request = ReadRequest()
        request.uid.value = str(UUID(int=42))
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.read(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(
            context.mock_calls, [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))]
        )

    def test_read_no_file(self):
        # Test case, when the file is not found in the storage.
        uuid = UUID(int=42)
        path = "example-path.txt"
        afile = File(uuid=uuid, path=path, size=1024, name="example.txt")
        self.session.add(afile)
        self.session.commit()

        request = ReadRequest()
        request.uid.value = str(UUID(int=42))
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.read(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(
            context.mock_calls,
            [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("File .* is not available: .*"))],
        )

    def _test_read_not_found(self, raw_id: str) -> None:
        request = ReadRequest()
        request.uid.value = raw_id
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.read(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.NOT_FOUND, f"File '{raw_id}' not found")])

    def test_read_not_found(self):
        # Test ID can be resolved, but file does not exist.
        self._test_read_not_found("255")

    def test_read_invalid(self):
        # Test ID can't be resolved.
        self._test_read_not_found("invalid")

    def _test_read(self, content: bytes, replies: Iterable[ReadReply], size: int = 0) -> None:
        uuid = UUID(int=42)
        path = "example-path.txt"
        with open(os.path.join(self.tmp_dir.name, path), mode="wb") as tmp_file:
            tmp_file.write(content)
        afile = File(uuid=uuid, path=path, size=1024, name="example.txt")
        self.session.add(afile)
        self.session.commit()

        request = ReadRequest()
        request.uid.value = str(uuid)
        request.size = size
        result = self.service.read(request, sentinel.context)

        self.assertEqual(tuple(result), replies)

    def test_read(self):
        reply = ReadReply()
        reply.data.data = self.content
        self._test_read(self.content, (reply,))

    def test_read_numeric_id(self):
        id = 42
        path = "example-path.txt"
        with open(os.path.join(self.tmp_dir.name, path), mode="wb") as tmp_file:
            tmp_file.write(self.content)
        afile = File(id=id, path=path, size=1024, name="example.txt")
        self.session.add(afile)
        self.session.commit()

        request = ReadRequest()
        request.uid.value = str(id)
        result = self.service.read(request, sentinel.context)

        reply = ReadReply()
        reply.data.data = self.content
        self.assertEqual(tuple(result), (reply,))

    def test_read_chunked(self):
        reply1 = ReadReply()
        reply1.data.data = self.content[:12]
        reply2 = ReadReply()
        reply2.data.data = self.content[12:]
        self._test_read(self.content, replies=(reply1, reply2), size=12)

    def test_read_empty_file(self):
        reply = ReadReply()
        reply.data.data = b""
        self._test_read(b"", (reply,))

    def test_read_supersized(self):
        # Test unreasonably big size works as well.
        reply = ReadReply()
        reply.data.data = self.content
        self._test_read(self.content, (reply,), size=sys.maxsize + 1)

    def _test_create_error(self, requests: Iterable[CreateRequest], status: StatusCode, error_msg: str) -> None:
        context = Mock(name="context")
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.create(iter(requests), context)

        self.assertEqual(catcher.exception.args, (sentinel.abort,))
        self.assertEqual(context.mock_calls, [call.abort(status, error_msg)])

    def test_create_empty(self):
        request = CreateRequest()
        self._test_create_error((request,), StatusCode.INVALID_ARGUMENT, "'' is not a valid name")

    def test_create_empty_stream(self):
        # Test no requests in stream.
        self._test_create_error((), StatusCode.INVALID_ARGUMENT, "No data provided.")

    def test_create_invalid_mimetype(self):
        request = CreateRequest()
        request.name = "example.txt"
        request.mimetype = "invalid/mimetype"
        self._test_create_error((request,), StatusCode.INVALID_ARGUMENT, "'invalid/mimetype' is not a valid MIME type")

    def test_create_mismatch_name(self):
        # Test error is returned if name changes.
        request_1 = CreateRequest()
        request_1.name = "example.txt"
        request_2 = CreateRequest()
        request_2.name = "example_2.txt"
        self._test_create_error(
            (request_1, request_2),
            StatusCode.INVALID_ARGUMENT,
            "Name mismatch detected: 'example.txt' != 'example_2.txt'",
        )

    def test_create_mismatch_mimetype(self):
        # Test error is returned if mimetype changes.
        request_1 = CreateRequest()
        request_1.name = "example.txt"
        request_1.mimetype = "text/plain"
        request_2 = CreateRequest()
        request_2.mimetype = "plain"
        self._test_create_error(
            (request_1, request_2), StatusCode.INVALID_ARGUMENT, "Mimetype mismatch detected: 'text/plain' != 'plain'"
        )

    def test_create_db_error(self):
        # Test DB error occurs on database level - dropped table
        with self.engine.begin() as conn:
            BaseModel.metadata.drop_all(bind=conn)  # type: ignore[attr-defined]
        request = CreateRequest()
        request.name = "example.txt"
        self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))

    def test_create_error_storage(self):
        # Test creation of directory failed.
        Path(self.tmp_dir.name).chmod(0o000)  # Drop permissions for the directory.
        request = CreateRequest()
        request.name = "example.txt"
        with freeze_time("2020-05-02 00:00:00"):
            with non_root_user():
                self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "Can't create directory.")

    def test_create_error_directory(self):
        # Test creation of subdirectory failed.
        storage = Path(self.tmp_dir.name)
        storage.chmod(0o777)
        (storage / "2020").mkdir(0o000)  # Drop permissions for the subdirectory.
        request = CreateRequest()
        request.name = "example.txt"
        with freeze_time("2020-05-02 00:00:00"):
            with non_root_user():
                self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "Can't create directory.")

    def test_create_error_file(self):
        # Test creation of file failed.
        storage = Path(self.tmp_dir.name)
        storage.chmod(0o777)
        dir_path = storage / "2020" / "05" / "02"
        dir_path.mkdir(parents=True)
        dir_path.chmod(0o000)  # Drop permissions for the final directory.
        request = CreateRequest()
        request.name = "example.txt"
        with freeze_time("2020-05-02 00:00:00"):
            with non_root_user():
                self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "Can't create file.")

    def test_create_error_file_exists(self):
        # Test file already exist.
        uuid = UUID(int=42)
        self.create_storage_file(Path("2020") / "05" / "02" / str(uuid))
        request = CreateRequest()
        request.name = "example.txt"
        with freeze_time("2020-05-02 00:00:00"):
            with patch("fileman.service.uuid4", return_value=uuid):
                self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "File already exist.")

    def test_create_error_move_failed(self):
        # Test move of the file to target location failed.
        request = CreateRequest()
        request.name = "example.txt"
        with patch("fileman.service.shutil.move", side_effect=OSError("Gazpacho!")):
            self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "File creation failed: Gazpacho!")

    def test_create_error_failed_unlink(self):
        # Test deletion of temporary file failed after an error.
        request = CreateRequest()
        request.name = "example.txt"
        with patch("fileman.service.shutil.move", side_effect=OSError("Gazpacho!")):
            with patch("fileman.service.Path.unlink", side_effect=FileNotFoundError):
                self._test_create_error((request,), StatusCode.FAILED_PRECONDITION, "File creation failed: Gazpacho!")

    def _test_create(self, requests: Iterable[CreateRequest], content: bytes) -> File:
        result = self.service.create(iter(requests), sentinel.context)

        # Check file is created
        file = cast(File, self.session.query(File).one())
        self.assertTrue(file.path)
        self.assertEqual(file.size, len(content))
        # Check reply
        reply = CreateReply()
        reply.data.uid.value = str(file.uuid)
        self.assertEqual(result, reply)
        # Check content of the file at the file system.
        # TODO: Drop 'str', once 3.6 is the minimal python supported.
        with open(str(Path(self.tmp_dir.name) / file.path), "rb") as stored_file:
            self.assertEqual(stored_file.read(), content)

        return file

    def test_create_empty_file(self):
        name = "example.txt"
        request = CreateRequest()
        request.name = name

        file = self._test_create((request,), b"")

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, "application/octet-stream")

    def test_create(self):
        name = "example.txt"
        request = CreateRequest()
        request.name = name
        request.data = self.content

        file = self._test_create((request,), self.content)

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, "application/octet-stream")

    def test_create_mimetype(self):
        name = "example.txt"
        mimetype = "text/plain"
        request = CreateRequest()
        request.name = name
        request.mimetype = mimetype

        file = self._test_create((request,), b"")

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, mimetype)

    def test_create_chunked(self):
        name = "example.txt"
        request_1 = CreateRequest()
        request_1.name = name
        request_1.data = self.content[:12]
        request_2 = CreateRequest()
        request_2.name = name
        request_2.data = self.content[12:]

        file = self._test_create((request_1, request_2), self.content)

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, "application/octet-stream")

    def test_create_chunked_no_name(self):
        # Test no name in subsequent messages.
        name = "example.txt"
        request_1 = CreateRequest()
        request_1.name = name
        request_1.data = self.content[:12]
        request_2 = CreateRequest()
        request_2.data = self.content[12:]

        file = self._test_create((request_1, request_2), self.content)

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, "application/octet-stream")

    def test_create_chunked_no_mimetype(self):
        # Test no mimetype in subsequent messages.
        name = "example.txt"
        mimetype = "text/plain"
        request_1 = CreateRequest()
        request_1.name = name
        request_1.mimetype = mimetype
        request_1.data = self.content[:12]
        request_2 = CreateRequest()
        request_2.data = self.content[12:]

        file = self._test_create((request_1, request_2), self.content)

        self.assertEqual(file.name, name)
        self.assertEqual(file.mimetype, mimetype)
