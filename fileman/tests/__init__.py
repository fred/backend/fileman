from ..settings import APPLICATION

# Load settings for tests. Sadly there's no better place to put this.
if not APPLICATION.is_loaded:  # pragma: no cover
    APPLICATION.load()
