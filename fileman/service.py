"""Fileman gRPC services."""

import itertools
import logging
import mimetypes
import os
import shutil
import sys
from collections.abc import Iterator
from datetime import date
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Optional, cast
from uuid import UUID, uuid4

from fred_api.fileman.service_fileman_grpc_pb2 import (
    CreateReply,
    CreateRequest,
    ReadReply,
    ReadRequest,
    StatReply,
    StatRequest,
)
from fred_api.fileman.service_fileman_grpc_pb2_grpc import FileServicer as BaseFileServicer
from frgal.exceptions import ServiceError
from frgal.service import wrap_stream, wrap_unary
from grpc import StatusCode
from grpc._server import _Context
from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.orm.exc import NoResultFound

from .models import File

_LOGGER = logging.getLogger(__name__)


class FileServicer(BaseFileServicer):
    """File gRPC service.

    Attributes:
        storage: A path to the root of the storage.
        session_cls: SQLAlchemy Session class to be used for queries.
    """

    def __init__(self, storage: str, session_cls: Optional[Session] = None):
        self.storage = Path(storage)
        self.session_cls = session_cls or sessionmaker(future=True)

    def _get_file(self, session: Session, raw_id: str) -> File:
        """Return a file model identified by a raw_id."""
        query = session.query(File)
        try:
            uuid = UUID(raw_id)
        except ValueError:
            try:
                id = int(raw_id)
            except ValueError as error:
                # Can't resolve ID. Return not found.
                raise ServiceError(StatusCode.NOT_FOUND, "File {!r} not found".format(raw_id)) from error
            else:
                query = query.filter_by(id=id)
        else:
            query = query.filter_by(uuid=uuid)

        try:
            return cast(File, query.one())
        except NoResultFound as error:
            raise ServiceError(StatusCode.NOT_FOUND, "File {!r} not found".format(raw_id)) from error

    @wrap_unary
    def stat(self, request: StatRequest, context: _Context) -> StatReply:
        """Return file metadata."""
        session = self.session_cls()
        try:
            file = self._get_file(session, request.uid.value)
        except DBAPIError as error:
            _LOGGER.error("Error in SQL query: %s", error)
            raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error
        finally:
            session.close()
        reply = StatReply()
        reply.data.create_datetime.FromDatetime(file.create_datetime)
        reply.data.size = file.size
        reply.data.mimetype = file.mimetype
        reply.data.name = file.name
        return reply

    def _get_chunk_size(self, size: int, file_size: int) -> int:
        """Return a reasonable chunk size.

        Arguments:
            size: Requested chunk size
            file_size: Actual size of a requested file
        """
        if not size:
            # Size not set, return everything.
            return -1
        else:
            # Limit requested size to the actual file size. Don't go over sys.maxsize, may cause as overflow error.
            return min(size, file_size, sys.maxsize)

    @wrap_stream
    def read(self, request: ReadRequest, context: _Context) -> ReadReply:
        """Return file content."""
        session = self.session_cls()
        try:
            file = self._get_file(session, request.uid.value)
        except DBAPIError as error:
            _LOGGER.error("Error in SQL query: %s", error)
            raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error
        finally:
            session.close()

        try:
            path = self.storage.joinpath(file.path)
            # XXX: Explicitly convert Path to str to support Python 3.5. `getsize` accepts Path since 3.6.
            file_size = os.path.getsize(str(path))
            chunk_size = self._get_chunk_size(request.size, file_size)
            # XXX: Explicitly convert Path to str to support Python 3.5. `open` accepts Path since 3.6.
            with open(str(path), mode="rb") as reader:
                while True:
                    reply = ReadReply()
                    chunk = reader.read(chunk_size)
                    reply.data.data = chunk
                    yield reply

                    if chunk_size == -1 or chunk_size == file_size or len(chunk) < chunk_size:
                        # If the file is read, terminate the generator.
                        return
        except OSError as error:
            _LOGGER.error("OS error: %s", error)
            raise ServiceError(
                StatusCode.FAILED_PRECONDITION, "File {} is not available: {}".format(file.uuid, error)
            ) from error

    def _check_create_args(self, request: CreateRequest, name: str, mimetype: str) -> None:
        """Ensure request arguments don't mismatch.

        Arguments:
            request: A new request from stream.
            name: The name of the file.
            mimetype: The mimetype of the file.

        Raises:
            ServiceError: If a mismatch in name or mimetype is found.
        """
        if request.name and request.name != name:
            raise ServiceError(
                StatusCode.INVALID_ARGUMENT, "Name mismatch detected: {!r} != {!r}".format(name, request.name)
            )
        if request.mimetype and request.mimetype != mimetype:
            raise ServiceError(
                StatusCode.INVALID_ARGUMENT,
                "Mimetype mismatch detected: {!r} != {!r}".format(mimetype, request.mimetype),
            )

    def _create_storage_file(self, full_path: Path) -> None:
        """Create an empty file in the storage.

        All intermediate directories are created.

        Arguments:
            full_path: An absolute path of the file.

        Raises:
            ServiceError: If an error occurs.
        """
        try:
            full_path.parent.mkdir(parents=True, exist_ok=True)
        except OSError as error:
            raise ServiceError(StatusCode.FAILED_PRECONDITION, "Can't create directory.") from error
        try:
            full_path.touch(exist_ok=False)
        except FileExistsError as error:
            raise ServiceError(StatusCode.FAILED_PRECONDITION, "File already exist.") from error
        except OSError as error:
            raise ServiceError(StatusCode.FAILED_PRECONDITION, "Can't create file.") from error

    @wrap_unary
    def create(self, requests: Iterator[CreateRequest], context: _Context) -> CreateReply:
        """Create a new file."""
        # Setup process from the first request.
        try:
            head_request = next(requests)
        except StopIteration as error:
            # No requests, fail now.
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "No data provided.") from error
        name = head_request.name
        mimetype = head_request.mimetype
        if not name:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "'{}' is not a valid name".format(name))
        if mimetype and mimetype not in mimetypes.types_map.values():
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "{!r} is not a valid MIME type".format(mimetype))

        # Prepend the first request back.
        requests = itertools.chain((head_request,), requests)

        session = self.session_cls()
        with NamedTemporaryFile(delete=False) as writer:
            try:
                # Write file content to a file system.
                for request in requests:
                    self._check_create_args(request, name, mimetype)
                    writer.write(request.data)
                # Ensure all data are written.
                writer.flush()

                # Create the file instance.
                today = date.today()
                uuid = uuid4()
                path = today.strftime("%Y/%m/%d") + "/" + str(uuid)
                size = Path(writer.name).stat().st_size
                file = File(name=name, size=size, path=path, uuid=uuid)
                if mimetype:
                    file.mimetype = mimetype
                session.add(file)
                # Force the INSERT execution. If an error is encountered now, temporary file will be deleted.
                session.flush()

                # Move the file to the target location.
                full_path = self.storage / path
                self._create_storage_file(full_path)
                # TODO: Drop 'str', once 3.6 is the minimal python supported.
                shutil.move(writer.name, str(full_path))

                session.commit()
            except DBAPIError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error
            except OSError as error:
                _LOGGER.error("OS error on file %s creation: %s", name, error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "File creation failed: {}".format(error)) from error
            finally:
                session.close()
                # Ensure the temporary file is deleted.
                # TODO: Use 'missing_ok' argument, once 3.8 is the minimal python supported.
                try:
                    Path(writer.name).unlink()  # pragma: no branch # unlink always fails on successful file creation.
                except FileNotFoundError:
                    # Ignore if file doesn't exist.
                    pass

        reply = CreateReply()
        reply.data.uid.value = str(uuid)
        return reply
