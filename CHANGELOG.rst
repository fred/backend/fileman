ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

0.3.0 (2023-11-08)
------------------

* Drop support for python 3.7.
* Add Support for python 3.11 and 3.12.
* Switch to fileman API 1.2 (#13).
* Drop support for SQLAlchemy 1.3 (#6).
* Use SQLAlchemy 2.0 API (#4).
* Resolve SQLAlchemy 2.0 warnings (#5).
* Replace EasySettings with pydantic (#7).
* Support numeric ID as file UID (#10).
* Check mime type in create (#9).
* Fix annotations.
* Update project setup.

0.2.0 (2022-02-23)
------------------

* Support SQLAlchemy 1.4.
* Add support for python 3.9 and 3.10.
* Drop support for python 3.5 and 3.6.
* Add ``create`` method to service.
* Refactor Sentry settings.
* Use service utilities from ``frgal``.
* Improve coverage.
* Add man pages.
* Update static checks, especially mypy, and project setup.

0.1.0 (2020-06-16)
------------------

Initial version.

* Add read-only access to files.
