fred-fileman-server
===================

Runs ``EmailMessenger`` or ``SmsMessenger`` gRPC services.

Synopsis
--------

``fred-fileman-server [OPTIONS]``

Options
^^^^^^^^

.. option:: --type [all|email|sms|letter]

   Run only service with selected type.

   Default: all

.. option:: -p, --port INTEGER

   Set custom port.

.. option:: --grpc-options TEXT

   Set custom options to the gRPC server,
   can be used multiple times.

.. option:: --connection TEXT

   Set custom database connection string.

.. option:: --config FILE

   Set custom config file.

.. option:: --version

   Show the version and exit.
  
.. option:: --help

   Show this message and exit.

Example
^^^^^^^^

``fred-fileman-server -p 2``

