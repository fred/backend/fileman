.. fred-fileman documentation master file, created by
   sphinx-quickstart on Mon Jul 22 20:45:40 2019.

Fileman Manual
==============

.. toctree::
   :maxdepth: 2
   :caption: Commands:

   server
   
..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
